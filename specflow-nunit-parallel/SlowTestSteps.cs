﻿using System;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using TechTalk.SpecFlow;

namespace specflow_nunit_parallel
{
    [Binding]
    public class TestFeature1Steps
    {
        [Given(@"I go to ""(.*)""")]
        public void GivenIGoTo(string url)
        {
            var capabilities = new DesiredCapabilities();

            capabilities = DesiredCapabilities.Chrome();
            capabilities.SetCapability(CapabilityType.BrowserName, "chrome");
            capabilities.SetCapability(CapabilityType.Platform, new Platform(PlatformType.Any));
            capabilities.SetCapability(CapabilityType.Version, "");

            using (var driver = new RemoteWebDriver(new Uri("http://localhost:4444/wd/hub"), capabilities))
            {
                driver.Navigate().GoToUrl(url);

                driver.Dispose();
            }
        }

        [Then(@"I wait")]
        public void ThenIWait()
        {
            Thread.Sleep(30000);
        }
    }
}