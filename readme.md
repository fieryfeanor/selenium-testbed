# Instructions for running the demo

1. Download [selenium server stand alone 3.3](http://selenium-release.storage.googleapis.com/3.3/selenium-server-standalone-3.3.1.jar) (3.4 has issues apparently)

2. Download latest [chrome driver](https://chromedriver.storage.googleapis.com/2.30/chromedriver_win32.zip)


Then unpack chrome driver and place selenium server into the same folder

Next run the following commands

1. `selenium-server-standalone-3.3.1.jar -role hub`

2. `java -Dwebdriver.chrome.driver=c:/temp/selenium/chromedriver.exe -jar selenium-server-standalone-3.3.1.jar -role node -hub http://localhost:4444/grid/register`

**Make sure you unblock both of these files!**

You can verify the node has connected to the hub at [this url](http://localhost:4444/grid/console)